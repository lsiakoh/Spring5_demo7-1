package com.lyp.spring5.dao;

public interface UserDao {
    //多钱方法
    public void addMoney();

    //少钱方法
    public void reduceMoney();
}
