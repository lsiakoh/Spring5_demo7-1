package com.lyp.spring5.service;


import com.lyp.spring5.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = false ,timeout = -1,propagation = Propagation.REQUIRED , isolation = Isolation.REPEATABLE_READ)
public class UserService {

    //注入dao
    @Autowired
    private UserDao userDao;

    //转账方法
    public void accountMoney(){
//        try{
            //第一步  开启事物操作

            //第二部 进行业务操作

            //lucy 少 100
            userDao.reduceMoney();
            //模拟异常
            int i = 10 / 0;
            //mary 多 100
            userDao.addMoney();

            //第三步 没有发生异常 提交事务

//        }catch (Exception e){
            // 第四步  出现异常  事物回滚
//        }

    }

}
