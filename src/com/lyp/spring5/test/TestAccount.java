package com.lyp.spring5.test;

import com.lyp.spring5.config.TxCinfig;
import com.lyp.spring5.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.lang.management.PlatformLoggingMXBean;

public class TestAccount {

    @Test
    public void TestAcount(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("bean1.xml");
        UserService userService = context.getBean("userService", UserService.class);
        userService.accountMoney();
    }


    @Test
    public void TestAcount1(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(TxCinfig.class);
        UserService userService = context.getBean("userService", UserService.class);
        userService.accountMoney();
    }
}
